import type { IControllerOptions, IMethodOptions } from './interface';

import { INJECT_TYPE } from '../common/constants';
import { EScope } from '../container';
import { CONTROLLER_METHOD_OPTIONS, CONTROLLER_OPTIONS, INJECT_TYPE_CONTROLLER } from './constants';

export const Controller = (options: IControllerOptions): ClassDecorator => {
  return <T extends Function>(target: T) => {
    const optionsWithDefaults: IControllerOptions = {
      id: options.id,
      path: options.path,
      scope: options.scope ?? EScope.Singleton,
    };

    Reflect.defineMetadata(
      CONTROLLER_OPTIONS,
      [...(Reflect.getMetadata(CONTROLLER_OPTIONS, target) ?? []), optionsWithDefaults],
      target,
    );

    Reflect.defineMetadata(INJECT_TYPE, INJECT_TYPE_CONTROLLER, target);
  };
};

export const Event = (options: IMethodOptions): MethodDecorator => {
  return (target, prop, descriptor: TypedPropertyDescriptor<any>) => {
    const optionsWithDefaults: IMethodOptions = {
      controllerId: options.controllerId,
      path: options.path,
      meta: options.meta,
    };

    Reflect.defineMetadata(
      CONTROLLER_METHOD_OPTIONS,
      [...(Reflect.getMetadata(CONTROLLER_METHOD_OPTIONS, descriptor.value) ?? []), optionsWithDefaults],
      descriptor.value,
    );
  };
};
