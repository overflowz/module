export const INJECT_TYPE_CONTROLLER = Symbol('inject:type:controller');

export const CONTROLLER_OPTIONS = Symbol('controller:options');
export const CONTROLLER_METHOD_OPTIONS = Symbol('controller:options:options');
