import type { EScope } from '../container';

export interface IControllerOptions {
  id: unknown;
  path: string;
  scope?: EScope;
}

export interface IMethodOptions {
  controllerId: unknown;
  path: string;
  meta?: unknown;
}
