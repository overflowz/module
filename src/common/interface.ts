import type { IApplicationContext } from '../app';

export interface IStaticHooks {
  beforeLoad?: (context: IApplicationContext) => any
}

export type TClass = (new (...args: any[]) => any) & IStaticHooks;
export type Nullable<T> = T | undefined | null;
