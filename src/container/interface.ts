export enum EScope {
  Singleton = 'Singleton',
  Request = 'Request',
  Transient = 'Transient',
}
