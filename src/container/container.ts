import {
  BindingScopeEnum, Container as InversifyContainer, decorate, injectable, interfaces, METADATA_KEY,
} from 'inversify';

import { EScope } from './interface';

import type { TClass } from '../common';

export class Container {
  readonly #container: InversifyContainer;
  readonly #scopeMap = {
    [EScope.Singleton]: (target: interfaces.BindingInWhenOnSyntax<any>) => target.inSingletonScope(),
    [EScope.Transient]: (target: interfaces.BindingInWhenOnSyntax<any>) => target.inTransientScope(),
    [EScope.Request]: (target: interfaces.BindingInWhenOnSyntax<any>) => target.inRequestScope(),
  };

  constructor() {
    this.#container = new InversifyContainer({
      autoBindInjectable: false,
      defaultScope: BindingScopeEnum.Singleton,
    });
  }

  private applyScope(target: interfaces.BindingInWhenOnSyntax<any>, scope: EScope) {
    if (!Reflect.has(this.#scopeMap, scope)) {
      throw new Error(`invalid scope ${scope}`);
    }

    Reflect.apply(this.#scopeMap[scope], this, [target]);
  }

  setParent(container: Container) {
    this.#container.parent = container.#container;
  }

  get<T extends TClass>(target: T): InstanceType<T> {
    if (this.has(target)) {
      return this.#container.get(target);
    }

    throw new Error(`${target.name} is not registered`);
  }

  has<T extends TClass>(target: T): boolean {
    return this.#container.isBound(target);
  }

  registerClass<T extends TClass | Function>(target: T, scope: EScope): void {
    if (!Reflect.hasMetadata(METADATA_KEY.PARAM_TYPES, target)) {
      decorate(injectable(), target);
    }

    this.applyScope(
      this.#container.bind(target).toSelf(),
      scope,
    );
  }

  registerFactory<T extends TClass | Function>(identifier: T, factory: () => any, scope: EScope) {
    this.applyScope(
      this.#container.bind(identifier).toDynamicValue(() => factory()),
      scope,
    );
  }
}
