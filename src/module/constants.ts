export const MODULE_CONTAINER = Symbol('module:container');

export const MODULE_IMPORTS = Symbol('module:imports');
export const MODULE_EXPORTS = Symbol('module:exports');
export const MODULE_PROVIDERS = Symbol('module:providers');
export const MODULE_CONTROLLERS = Symbol('module:controllers');

export const INJECT_TYPE_MODULE = Symbol('inject:type:module');
