import type { TClass } from '../common';

export interface IModuleOptions {
  imports?: TClass[];
  providers?: TClass[];
  controllers?: TClass[];
  exports?: TClass[];
}

export interface OnModuleInit {
  onModuleInit(): any;
}
