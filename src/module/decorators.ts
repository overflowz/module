import { INJECT_TYPE } from '../common/constants';
import { Container } from '../container/container';
import {
  INJECT_TYPE_MODULE, MODULE_CONTAINER, MODULE_CONTROLLERS, MODULE_EXPORTS, MODULE_IMPORTS,
  MODULE_PROVIDERS,
} from './constants';

import type { IModuleOptions } from './interface';

export const Module = (options?: IModuleOptions): ClassDecorator => {
  return <T extends Function>(target: T) => {
    const container: Container = new Container();

    Reflect.defineMetadata(INJECT_TYPE, INJECT_TYPE_MODULE, target);
    Reflect.defineMetadata(MODULE_CONTAINER, container, target);

    Reflect.defineMetadata(MODULE_IMPORTS, options?.imports ?? [], target);
    Reflect.defineMetadata(MODULE_EXPORTS, options?.exports ?? [], target);
    Reflect.defineMetadata(MODULE_PROVIDERS, options?.providers ?? [], target);
    Reflect.defineMetadata(MODULE_CONTROLLERS, options?.controllers ?? [], target);
  };
};
