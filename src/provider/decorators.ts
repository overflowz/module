import { INJECT_TYPE } from '../common/constants';
import { INJECT_TYPE_PROVIDER, PROVIDER_OPTIONS } from './constants';

import type { IProviderOptions } from './interface';

export const Injectable = (options?: IProviderOptions): ClassDecorator => {
  return <T extends Function>(target: T) => {
    Reflect.defineMetadata(PROVIDER_OPTIONS, options ?? {}, target);
    Reflect.defineMetadata(INJECT_TYPE, INJECT_TYPE_PROVIDER, target);
  };
};
