import { EScope } from '../container';

export interface IProviderOptions {
  scope?: EScope;
}
