import 'reflect-metadata';

import { INJECT_TYPE } from '../common/constants';
import { EScope } from '../container/interface';
import { CONTROLLER_METHOD_OPTIONS, CONTROLLER_OPTIONS } from '../controller/constants';
import {
  INJECT_TYPE_MODULE, MODULE_CONTAINER, MODULE_CONTROLLERS, MODULE_EXPORTS, MODULE_IMPORTS,
  MODULE_PROVIDERS,
} from '../module/constants';
import { INJECT_TYPE_PROVIDER, PROVIDER_OPTIONS } from '../provider/constants';
import {
  EResolutionType, IApplicationContext, ICreateApplicationOptions, IHandler,
} from './interface';

import type { Container } from '../container/container';
import type { TClass, Nullable } from '../common';
import type { IControllerOptions, IMethodOptions } from '../controller/interface';
import type { IProviderOptions } from '../provider/interface';

export class ApplicationContext implements IApplicationContext {
  private readonly container: Container;
  private readonly imports: TClass[];
  private readonly controllers: TClass[];
  private readonly exports: TClass[];

  constructor(
    private readonly module: TClass,
    private readonly followImports: boolean = false,
    private readonly resolutionType: EResolutionType,
  ) {
    this.container = Reflect.getMetadata(MODULE_CONTAINER, module);
    this.imports = Reflect.getMetadata(MODULE_IMPORTS, module);
    this.controllers = Reflect.getMetadata(MODULE_CONTROLLERS, module);
    this.exports = Reflect.getMetadata(MODULE_EXPORTS, module);
  }

  private internalGetHandlers(paths: IHandler[], controllerId: IControllerOptions['id']): IHandler[] {
    const result = [...paths];

    for (const controller of this.controllers) {
      const controllerOptions: IControllerOptions[] = Reflect.getMetadata(CONTROLLER_OPTIONS, controller);
      if (typeof controllerId !== 'undefined' && !controllerOptions.some(options => options.id === controllerId))
        continue;

      for (const name of Object.getOwnPropertyNames(controller.prototype)) {
        const method = controller.prototype[name];
        if (typeof method !== 'function') continue;

        const methodOptions: IMethodOptions[] = Reflect.getMetadata(CONTROLLER_METHOD_OPTIONS, method);
        if (!methodOptions) continue;

        // merge method/controllers which has same ids.
        const mergedMethodOptions: IMethodOptions[] = controllerOptions
          .reduce<IMethodOptions[]>((acc, currentControllerOptions) => {
            if (typeof controllerId !== 'undefined' && currentControllerOptions.id !== controllerId)
              return acc;

            const matchedMethods = methodOptions
              .filter(options => options.controllerId === currentControllerOptions.id)
              .map(options => ({ ...options, path: currentControllerOptions.path + options.path }));

            return [...acc, ...matchedMethods];
          }, []);

        for (const options of mergedMethodOptions) {
          result.push({
            ...options,
            controller,
            instance: () => {
              // NOTES: redefining metadata keys for now (hacky).
              // TODO: find a better way to expose metadata keys.
              const metaKeys = Reflect.getOwnMetadataKeys(method);
              const instance = method.bind(this.container.get(controller));

              for (const key of metaKeys) {
                Reflect.defineMetadata(key, Reflect.getMetadata(key, method), instance);
              }

              return instance;
            }
          });
        }
      }
    }

    if (this.followImports) {
      for (const mod of this.imports) {
        result.push(
          ...this.select(mod)!.internalGetHandlers(result, controllerId),
        );
      }
    }

    return result;
  }

  deep(): ApplicationContext {
    return new ApplicationContext(this.module, true, this.resolutionType);
  }

  strict(): ApplicationContext {
    return new ApplicationContext(this.module, false, this.resolutionType);
  }

  get<T extends TClass>(target: T): Nullable<InstanceType<T>> {
    // check root module first
    if (this.container.has(target)) {
      // always resolve if resolution type is hierarchical.
      // if it's isolated, check if target is in exports/controllers.
      const shouldTryGet = this.resolutionType === EResolutionType.Hierarchical ||
        (this.resolutionType === EResolutionType.Isolated && this.exports.includes(target) || this.controllers.includes(target));

      if (shouldTryGet) {
        return this.container.get(target);
      }
    }

    // check imports (if followImports are defined)
    if (this.followImports) {
      for (const mod of this.imports) {
        const match = this.select(mod)?.get(target);
        if (match) return match;
      }
    }
  }

  select<T extends TClass>(module: T): Nullable<ApplicationContext> {
    if (!this.imports.includes(module)) {
      return null;
    }

    return new ApplicationContext(module, this.followImports, this.resolutionType);
  }

  getHandlers(controllerId?: IControllerOptions['id']): IHandler[] {
    return this.internalGetHandlers([], controllerId);
  }

  getContainer(): Omit<Container, 'setParent'> {
    return this.container;
  }
}

class ModuleLoader {
  private readonly container: Container;
  private readonly parentContainer: Nullable<Container>;
  private readonly imports: TClass[];
  private readonly providers: TClass[];
  private readonly controllers: TClass[];
  private readonly exports: TClass[];

  constructor(
    private readonly applicationContext: ApplicationContext,
    private readonly module: TClass,
    private readonly parent?: TClass,
  ) {
    this.container = Reflect.getMetadata(MODULE_CONTAINER, module);
    this.parentContainer = Reflect.getMetadata(MODULE_CONTAINER, parent ?? {});
    this.imports = Reflect.getMetadata(MODULE_IMPORTS, module);
    this.providers = Reflect.getMetadata(MODULE_PROVIDERS, module);
    this.controllers = Reflect.getMetadata(MODULE_CONTROLLERS, module);
    this.exports = Reflect.getMetadata(MODULE_EXPORTS, module);
  }

  private registerIfNotPresent(target: TClass, container: Container, scope: EScope) {
    if (!container.has(target)) {
      container.registerClass(target, scope);
    }
  }

  private async callBeforeLoadHook(target: TClass) {
    return target.beforeLoad?.(this.applicationContext);
  }

  private async initializeDependency(target: TClass) {
    return this.container.get(target).onModuleInit?.();
  }

  registerHierarchical() {
    // traverse over imports
    for (const mod of this.imports) {
      new ModuleLoader(
        this.applicationContext,
        mod,
        // if import module is also in the exports,
        // it is a module export and pass parent as a
        // root container (if present).
        this.exports.includes(mod) ? this.parent ?? this.module : this.module,
      ).registerHierarchical();
    }

    // register providers in the current module
    for (const provider of this.providers) {
      const options: IProviderOptions = Reflect.getMetadata(PROVIDER_OPTIONS, provider);
      this.registerIfNotPresent(provider, this.container, options.scope ?? EScope.Singleton);
    }

    // register controllers in the current module
    for (const controller of this.controllers) {
      const options: IControllerOptions = Reflect.getMetadata(CONTROLLER_OPTIONS, controller);
      this.registerIfNotPresent(controller, this.container, options.scope ?? EScope.Singleton);
    }

    // register module in the current container
    this.registerIfNotPresent(this.module, this.container, EScope.Singleton);

    // register exported providers in the parent container
    if (this.parentContainer) {
      // set parent container
      this.container.setParent(this.parentContainer);

      // filter only providers (exclude modules)
      const exportedProviders = this.exports
        .filter(moduleOrProvider => Reflect.getMetadata(INJECT_TYPE, moduleOrProvider) === INJECT_TYPE_PROVIDER);

      for (const provider of exportedProviders) {
        const options: IProviderOptions = Reflect.getMetadata(PROVIDER_OPTIONS, provider);
        this.registerIfNotPresent(provider, this.parentContainer, options.scope ?? EScope.Singleton);
      }
    }
  }

  registerIsolated() {
    // traverse over imports
    for (const mod of this.imports) {
      new ModuleLoader(
        this.applicationContext,
        mod,
        // if import module is also in the exports,
        // it is a module export and pass parent as a
        // root container (if present).
        this.exports.includes(mod) ? this.parent ?? this.module : this.module,
      ).registerIsolated();

      const exportedProviders: TClass[] = this.exports.filter(
        target => Reflect.getMetadata(INJECT_TYPE, target) === INJECT_TYPE_PROVIDER,
      );

      // TODO: is this correct?
      // register proxy between this and imported module for the provider
      // in a request scope.
      if (this.parentContainer) {
        for (const provider of exportedProviders) {
          this.parentContainer.registerFactory(
            provider,
            () => this.container.has(provider) ? this.container.get(provider) : undefined,
            EScope.Request,
          );
        }
      }
    }

    // register current module's providers
    for (const provider of this.providers) {
      const options: IProviderOptions = Reflect.getMetadata(PROVIDER_OPTIONS, provider);
      this.registerIfNotPresent(provider, this.container, options.scope ?? EScope.Singleton);
    }

    // register current module's controllers
    for (const controller of this.controllers) {
      const options: IControllerOptions = Reflect.getMetadata(CONTROLLER_OPTIONS, controller);
      this.registerIfNotPresent(controller, this.container, options.scope ?? EScope.Singleton);
    }

    // register module itself
    this.registerIfNotPresent(this.module, this.container, EScope.Singleton);
  }

  async loadHierarchical() {
    // call beforeLoad hook
    await this.callBeforeLoadHook(this.module);

    // traverse over imports
    for (const mod of this.imports) {
      await new ModuleLoader(
        this.applicationContext,
        mod,
        // if import module is also in the exports,
        // it is a module export and pass parent as a
        // root container (if present).
        this.exports.includes(mod) ? this.parent ?? this.module : this.module,
      ).loadHierarchical();
    }

    const importsReducer = (acc: TClass[], mod: TClass): TClass[] => {
      const exports: TClass[] = Reflect.getMetadata(MODULE_EXPORTS, mod);

      return exports.map(exp => {
        const injectType = Reflect.getMetadata(INJECT_TYPE, exp);

        switch (injectType) {
          case INJECT_TYPE_MODULE: return importsReducer(acc, exp);
          case INJECT_TYPE_PROVIDER: return [exp];
          default: return acc;
        }
      }).reduce((acc, x) => [...acc, ...x], []);
    };

    const importedProviders = this.imports
      .filter(mod => !this.exports.includes(mod))
      .reduce(importsReducer, []);

    for (const provider of importedProviders) {
      await this.initializeDependency(provider);
    }

    for (const provider of this.providers) {
      await this.initializeDependency(provider);
    }

    for (const controller of this.controllers) {
      await this.initializeDependency(controller);
    }

    await this.initializeDependency(this.module);
  }

  async loadIsolated() {
    // call beforeLoad hook
    await this.callBeforeLoadHook(this.module);

    // traverse over imports
    for (const mod of this.imports) {
      await new ModuleLoader(
        this.applicationContext,
        mod,
        // if import module is also in the exports,
        // it is a module export and pass parent as a
        // root container (if present).
        this.exports.includes(mod) ? this.parent ?? this.module : this.module,
      ).loadIsolated();
    }

    for (const provider of this.providers) {
      await this.initializeDependency(provider);
    }

    for (const controller of this.controllers) {
      await this.initializeDependency(controller);
    }

    await this.initializeDependency(this.module);
  }
}

export const createApplication = async (module: TClass, options?: ICreateApplicationOptions) => {
  const { resolutionType } = options ?? {
    resolutionType: EResolutionType.Hierarchical,
  };

  const app = new ApplicationContext(module, false, resolutionType);
  const loader = new ModuleLoader(app, module);

  switch (resolutionType) {
    case EResolutionType.Hierarchical: {
      loader.registerHierarchical(), await loader.loadHierarchical();
      break;
    }
    case EResolutionType.Isolated: {
      loader.registerIsolated(), await loader.loadIsolated();
      break;
    }
    default: {
      throw new Error('Invalid resolution type.');
    }
  }

  return app;
};
