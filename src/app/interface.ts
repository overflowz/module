import type { TClass, Nullable } from '../common';
import type { Container } from '../container/container';
import type { IMethodOptions } from '../controller';

export interface IHandler extends IMethodOptions {
  /**
   * controller that has metadata.
   */
  controller: TClass;
  /**
   * initiates controller & returns the handler that
   * can be executed.
   */
  instance(): (...args: any[]) => any;
}

export enum EResolutionType {
  Hierarchical,
  Isolated,
}

export interface IApplicationContext {
  deep(): IApplicationContext;
  strict(): IApplicationContext;
  get<T extends TClass>(target: T): Nullable<InstanceType<T>>;
  select<T extends TClass>(module: T): Nullable<IApplicationContext>;
  getContainer(): Omit<Container, 'setParent'>;
}

export interface ICreateApplicationOptions {
  resolutionType: EResolutionType;
}
